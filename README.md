# Clinical Trials History Scraper

A tool for mass-downloading of historical clinical trial data from
ClinicalTrials.gov and drks.de and a web app for visualizing
individual clinical trial registry entry changes over time

Run `app.R` to start the web app. You can adjust the limit on clinical
trial registry versions to download by editing the
`version_download_limit` variable on line 9 of `app.R`.

There is currently a live version of the web app here:

https://bgcarlisle.shinyapps.io/clinicaltrialshistoryscraper/

## `cthist`

The R package `cthist` enables mass-downloading of historical clinical
trial registry data and provides the data that this Shiny app
visualizes. It is available here:

https://github.com/bgcarlisle/cthist

## Citing Clinical Trials History Scraper

```
@Manual{bgcarlisle-ClinicalTrialsHistoryScraper,
  Title          = {Clinical Trials History Scraper},
  Author         = {Carlisle, Benjamin Gregory},
  Organization   = {The Grey Literature},
  Address        = {Berlin, Germany},
  url            = {https://codeberg.org/bgcarlisle/ClinicalTrialsHistoryScraper},
  year           = 2020
}
```

If you used this web app in your research and you found it useful, I
would take it as a kindness if you cited it.

Best,

Benjamin Gregory Carlisle PhD
